import logging

log = logging.getLogger(__name__)
from pyramid_handlers import action
from S4M_pyramid.lib.deprecated_pylons_globals import magic_globals, url, app_globals as g, config
from S4M_pyramid.lib.deprecated_pylons_abort_and_redirect import abort,redirect
from S4M_pyramid.lib.base import BaseController
from pyramid.renderers import render_to_response
import S4M_pyramid.lib.helpers as h
from S4M_pyramid.model.stemformatics import db_deprecated_pylons_orm as db, Stemformatics_Notification, Stemformatics_Help, Stemformatics_Gene, Stemformatics_Auth, Stemformatics_Dataset
import json
from pyramid.response import FileResponse
# Live querying
from S4M_pyramid.model.stemformatics import *

import re,os.path,subprocess

from sqlalchemy.exc import *


class DatasetsController(BaseController):
    __name__ = 'DatasetsController'


    def __init__(self,request):
        super().__init__(request)
        c = self.request.c
        self.human_db = config['human_db']
        self.mouse_db = config['mouse_db']
        c.human_db = self.human_db
        c.mouse_db = self.mouse_db


        self.default_human_dataset = int(config['default_human_dataset'])
        self.default_mouse_dataset = int(config['default_mouse_dataset'])

    @action()
    def pca(self):
        request = self.request
        c = self.request.c
        c.ds_id = request.params.get("ds_id")
        pca_type = request.params.get("pca_type")
        c.title = pca_type.replace("_"," ")
        # check if user has access to dataset
        status = Stemformatics_Dataset.check_dataset_with_limitations(db,c.ds_id,c.uid)
        if status == "Available":
            c.pca_type = request.params.get("pca_type")
            c.all_pca_types = Stemformatics_Dataset.get_pca_types_for_dataset(c.ds_id)
            return render_to_response('S4M_pyramid:templates/datasets/pca.mako', self.deprecated_pylons_data_for_view, request = self.request)
        else:
            return redirect(url(controller='contents', action='index'), code=404)

    @action()
    def return_pca_data(self):
        request = self.request
        response = self.request.response
        c = self.request.c
        file_name = request.params.get("file_name")
        c.ds_id =ds_id = request.params.get("ds_id") # check if user has access to data
        status = Stemformatics_Dataset.check_dataset_with_limitations(db,c.ds_id,c.uid)
        if status == "Available":
            pca_type = request.params.get("pca_type")
            data  = Stemformatics_Dataset.return_pca_data_files(ds_id,pca_type,file_name)
            return render_to_response('string',data,request = self.request)
        else:
            return redirect(url(controller='contents', action='index'), code=404)

    def search(self):
        c = self.request.c
        request = self.request
        response = self.request.response
        c.msc_values_access = config['msc_values_access']
        c.searchQuery = request.params.get("filter", None)

        if c.searchQuery:
            c.searchQuery = c.searchQuery.replace('<script>','').replace('</script>','')

        c.title = c.site_name+" - Dataset Search"
        ds_id = request.params.get("ds_id", None)
        c.selected_ds_id = False
        c.db_id = None
        c.dataset_status = ''

        if ds_id is not None and (c.searchQuery is None and c.searchQuery != ""):
            c.searchQuery = ds_id

        if ds_id is not None:
            db=None # this is not used at the moment
            dataset_status = Stemformatics_Dataset.check_dataset_with_limitations(db,ds_id,c.uid)
            if dataset_status == "Unavailable":
                return redirect(url(controller='contents', action='index'), code=404)
            if dataset_status == "Limited": # always show limited datasets
                pass

            ds_id = int(ds_id)
            c.ds_id = int(ds_id)
            dict_of_ds_ids = {}
            dict_of_ds_ids[ds_id] = {'dataset_status':dataset_status}
            format_type = 'front_end'
            c.dataset = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids,format_type)
            c.selected_ds_id = True
            # set the dataset data from the chosen ds_id into redis
            if c.dataset[ds_id]['has_data'] == 'yes':
                dataset_metadata = Stemformatics_Dataset.get_expression_dataset_metadata(ds_id)
            try:
                c.dataset_status = c.dataset[ds_id]['dataset_status']
            except:
                return redirect(url(controller='contents', action='index'), code=404)
            c.db_id = Stemformatics_Dataset.get_db_id(ds_id)
        else:
            c.ds_id = None
            c.datasets = None
            c.dataset = None
            c.selected_ds_id = False
        if c.selected_ds_id:
            c.header = Stemformatics_Notification.get_header_name_from_datasetId(c.ds_id)

        export = request.params.get("export",None)
        if export is None:
            if c.ds_id is not None:
                audit_dict = {'ref_type':'ds_id','ref_id':c.ds_id,'uid':c.uid,'url':url,'request':request}
                result = Stemformatics_Audit.add_audit_log(audit_dict)
            return render_to_response('S4M_pyramid:templates/datasets/search.mako',self.deprecated_pylons_data_for_view,request=self.request)
        else:
            # Task #396 - error with ie8 downloading with these on SSL
            response.headers.pop('Cache-Control', None)
            response.headers.pop('Pragma', None)

            response.headers['Content-type'] = 'text/tab-separated-values'
            stemformatics_version = config['stemformatics_version']
            response.headers['Content-Disposition'] = 'attachment;filename=export_stemformatics_'+stemformatics_version+'.tsv'
            response.charset= "utf8"
            ds_id = request.params.get("ds_id", None)

            if ds_id is not None:
                datasets = c.dataset
            else:
                filter_dict = {'show_limited':False,'rohart_msc_test':False}
                datasets = Stemformatics_Dataset.dataset_search(c.uid,c.searchQuery, filter_dict)

            data = self._convert_datasets_to_csv(ds_id,datasets)
            response.text = data
            return response

    def _convert_datasets_to_csv(self,ds_id,datasets):
        csv_text ="Title	Handle	Cells	Authors	PubmedID	Array Express	GEO	Genes of Interest	Contact Name	Contact Email	Affiliation	Platform	SRA	PXD	ENA\n"
        if datasets is None:
            return csv_text

        for temp_ds_id in datasets:
            if ds_id is not None and int(ds_id) != temp_ds_id:
               continue
            temp_row = datasets[temp_ds_id]
            title = temp_row['title']
            handle = temp_row['handle']
            cells_assayed = temp_row['cells_samples_assayed']
            authors =  temp_row['authors']
            description =  temp_row['description']
            pubmed_id =  temp_row['pub_med_id']
            AE =  temp_row['ae_accession_id']
            GEO =  temp_row['geo_accession_id']
            SRA =  temp_row['sra_accession_id']
            PXD =  temp_row['pxd_accession_id']
            ENA =  temp_row['ena_accession_id']
            genes_of_interest = temp_row['top_diff_exp_genes']
            if isinstance(genes_of_interest, dict):
                genes = []
                for symbol in genes_of_interest:
                    gene = genes_of_interest[symbol]['ensemblID']
                    genes.append(gene)
                genes_of_interest = ",".join(genes)

            contact_name = temp_row['name']
            contact_email =  temp_row['email']
            affiliation = temp_row['affiliation']
            platform =  temp_row['platform']
            probes_detected =  temp_row['probes detected']
            probes =  temp_row['probes']

            csv_text += title + "	" + handle + "	" + cells_assayed + "	" + authors + "	" + pubmed_id + "	" + AE + "	" + GEO + "	" + genes_of_interest + "	" + contact_name + "	" + contact_email + "	" + affiliation + "	" + platform + "	" + SRA + "	" + PXD    + "	" + ENA + "\n"
        return csv_text

    @action()
    def view(self):
        c = self.request.c
        c.title = c.site_name+" - Dataset Summary"
        ds_id = c.ds_id = int(self.request.matchdict['id'])
        return redirect('/datasets/search?ds_id='+str(ds_id))

    @action()
    def summary(self):
        request = self.request
        c = self.request.c
        c.title = c.site_name+" - Dataset Summary"
        try:
            ds_id = c.ds_id = int(request.params.get("datasetID", None))
        except:
            try:
                ds_id = c.ds_id = int(request.params.get("ds_id", None))
            except:
                return redirect(url(controller='contents', action='index'), code=404)

        return redirect('/datasets/search?ds_id='+str(ds_id))

    # Used in datasets/search for now
    @action(renderer = 'json')
    def get_details(self):
        c = self.request.c
        dataSets = Stemformatics_Dataset.getAllDatasetDetails(db,c.uid)
        if (dataSets == None): redirect(url(controller='contents', action='index'), code=404)
        return json.dumps(dataSets)

    @action()
    def download_yugene(self):
        request = self.request
        c = self.request.c
        response = self.request.response
        ds_id = int(self.request.matchdict['id'])

        export_key = request.params.get("export_key", None)
        username = request.params.get("username", None)
        if export_key is not None and username is not None and username is not u'':
            user = Stemformatics_Auth.get_user_from_username(db,username)
            uid = user.uid
            has_access = Stemformatics_Dataset.check_dataset_availability_by_export_key(db,uid,export_key,ds_id)
            if not has_access:
                return redirect(url(controller='contents', action='index'), code=404)
            else:
                permission_used = 'export_key'
                log_uid = uid
        else:
            has_access = Stemformatics_Dataset.check_dataset_availability(db,c.uid,ds_id)
            if not has_access:
                return redirect(url(controller='contents', action='index'), code=404)
            else:
                permission_used = 'logged_in'
                log_uid = c.uid




        download_type = 'Yugene'
        ip_address = request.environ.get("HTTP_X_FORWARDED_FOR", request.environ["REMOTE_ADDR"])
        result = Stemformatics_Dataset.audit_download_dataset(log_uid,ds_id,download_type,ip_address,permission_used)

        # read in the file
        file_name = config['x_platform_base_dir'] + 'dataset'+str(ds_id)+'.cumulative.txt'
        temp_file_name = config['DatasetTempGCTFiles'] + 'dataset'+str(ds_id)+'.tmpyugene'

        if os.path.isfile(file_name):
            sample_labels = Stemformatics_Expression.get_cumulative_sample_labels(ds_id)
            initial_header="Probe ID"
            new_header = Stemformatics_Expression.return_gct_file_sample_headers_as_replicate_group_id(db,ds_id,sample_labels,[],initial_header)

            new_header = new_header.replace("\n","")
            # changes for task 2563
            # http://stackoverflow.com/questions/28714142/getting-error-sed-e-expression-1-char-5-unknown-command-0
            command_line = "sed '1s\#.*\#"+new_header+"\#' "+file_name+" > "+temp_file_name
            p = subprocess.call(command_line,shell=True)
            response.headers.pop('Cache-Control', None)
            response.headers.pop('Pragma', None)
            response = FileResponse(temp_file_name, request=request, content_type='text/tab-separated-values')
            headers= response.headers
            headers['Content-Disposition'] = 'attachment;filename=dataset'+str(ds_id)+'.cumulative.txt'
            return response

        else:
            response.headers.pop('Cache-Control', None)
            response.headers.pop('Pragma', None)
            response.headers['Content-type'] = 'text/plain'
            response.headers['Content-Disposition'] = 'attachment;filename=dataset'+str(ds_id)+'.cumulative_error.txt'
            response.charset= "utf8"

            # push the file
            contents = 'There was no file to download. Please contact the '+c.site_name+' Team.'
            response.text = contents
            return response


    @action()
    def download_gct(self):
        request = self.request
        c = self.request.c
        response = self.request.response
        ds_id = int(self.request.matchdict['id'])
        export_key = request.params.get("export_key", None)
        username = request.params.get("username", None)
        if export_key is not None and username is not None and username is not u'':
            user = Stemformatics_Auth.get_user_from_username(db,username)
            uid = user.uid
            has_access = Stemformatics_Dataset.check_dataset_availability_by_export_key(db,uid,export_key,ds_id)
            if not has_access:
                return redirect(url(controller='contents', action='index'), code=404)
            else:
                permission_used = 'export_key'
                log_uid = uid
        else:
            has_access = Stemformatics_Dataset.check_dataset_availability(db,c.uid,ds_id)
            if not has_access:
                return redirect(url(controller='contents', action='index'), code=404)
            else:
                permission_used = 'logged_in'
                log_uid = c.uid


        download_type = 'Gct'
        ip_address = request.environ.get("HTTP_X_FORWARDED_FOR", request.environ["REMOTE_ADDR"])
        result = Stemformatics_Dataset.audit_download_dataset(log_uid,ds_id,download_type,ip_address,permission_used)

        # read in the file
        file_name = config['DatasetGCTFiles'] + 'dataset'+str(ds_id)+'.gct'
        temp_file_name = config['DatasetTempGCTFiles'] + 'dataset'+str(ds_id)+'.tmpexpression'

        if os.path.isfile(file_name):

            sample_labels = Stemformatics_Expression.get_sample_labels(ds_id)
            new_gct_header = Stemformatics_Expression.return_gct_file_sample_headers_as_replicate_group_id(db,ds_id,sample_labels,[])

            new_gct_header = new_gct_header.replace("\n","")
            # changes for task 2563
            # http://stackoverflow.com/questions/28714142/getting-error-sed-e-expression-1-char-5-unknown-command-0
            command_line = "sed '3s\#.*\#"+new_gct_header+"\#' "+file_name+" > "+temp_file_name
            p = subprocess.call(command_line,shell=True)
            response.headers.pop('Cache-Control', None)
            response.headers.pop('Pragma', None)
            response = FileResponse(temp_file_name, request=request, content_type='text/plain')
            headers= response.headers
            headers['Content-Disposition'] = 'attachment;filename=dataset'+str(ds_id)+'.gct'
            return response
            
        else:
            response.headers['Content-type'] = 'text/plain'
            response.headers['Content-Disposition'] = 'attachment;filename=dataset'+str(ds_id)+'_errords.gct'
            response.charset= "utf8"

            # push the file
            contents = 'There was no file to download. Please contact the '+c.site_name+' Team.'
            response.text = contact
            return response



    def download_cls(self):
        request = self.request
        c = self.request.c
        response = self.request.response
        id = int(self.request.matchdict['id'])
        has_access = Stemformatics_Dataset.check_dataset_availability(db,c.uid,id)
        if not has_access: redirect(url(controller='contents', action='index'), code=404)


        get_sortBy = request.params.get("sortBy")

        if get_sortBy is None:
            sort_by = ''
        else:
            sort_by = get_sortBy

        # read in the file
        file_name = config['DatasetCLSFiles'] + str(id) +sort_by+'.cls'
        f = open(file_name,'r')

        contents = f.read()
        response.headers.pop('Cache-Control', None)
        response.headers.pop('Pragma', None)
        response.headers['Content-type'] = 'text/plain'
        response.headers['Content-Disposition'] = 'attachment;filename=dataset'+str(id)+sort_by+'.cls'
        response.charset= "utf8"
        response.text = contents
        # push the file
        return response


    @action(renderer = 'string')
    def autocomplete_probes_for_dataset(self):
        request = self.request
        search_term = request.params.get("term")
        ds_id = request.params.get("ds_id")

        use_json = True
        result_json = Stemformatics_Dataset.get_autocomplete_probes_for_dataset(search_term,ds_id,use_json)
        return ''.join(result_json)

    @action()
    def download_ds_id_mapping_id_file(self):
        response = self.request.response
        ds_id = 0
        result = Stemformatics_Dataset.get_dataset_mapping_id(ds_id)
        data = Stemformatics_Dataset.text_for_download_ds_id_mapping_id_file(result)

        response.headers.pop('Cache-Control', None)
        response.headers.pop('Pragma', None)
        response.headers['Content-type'] = 'text/tab-separated-values'
        stemformatics_version = config['stemformatics_version']
        response.headers['Content-Disposition'] = 'attachment;filename=ds_id_to_mapping_id.tsv'
        response.charset= "utf8"

        response.text = data
        return response

    @action(renderer="string")
    def search_and_choose_datasets_ajax(self):
        c = self.request.c
        request = self.request
        temp_data = {}

        rohart_msc_test = request.params.get("rohart_msc_test", False)
        if rohart_msc_test == 'true':
            rohart_msc_test = True
        else:
            rohart_msc_test = False
        filter_dict = {'show_limited':False,'rohart_msc_test':rohart_msc_test}
        c.searchQuery = request.params.get("filter", None)

        temp_data = Stemformatics_Dataset.search_and_choose_datasets(c.uid,c.searchQuery,filter_dict)

        json_data = json.dumps(temp_data)

        audit_dict = {'ref_type':'search_term','ref_id':c.searchQuery,'uid':c.uid,'url':url,'request':request}
        result = Stemformatics_Audit.add_audit_log(audit_dict)

        return json_data
    
    @action(renderer="templates/datasets/summary_table.mako")
    def summary_table(self):
        request = self.request
        c = self.request.c
        c.title = c.site_name+" - Summary Table"

        c.ds_id = str(request.params.get('ds_id', ""))

        # c.query = self.request.POST.get('query')
        # c.columnName = self.request.POST.get('columnName')
        c.query = request.params.get('query')
        
        c.columnName = request.params.get('columnName')  
        if c.columnName:    
            dataset_id = request.params.get('ds_id') 
        else:
            dataset_id = c.ds_id 
        # print(c.query,c.columnName) 

        temp_data = []
        c.data_grouped = json.dumps(Stemformatics_Dataset.getGeoBiosamplesMetadata_grouped(db,dataset_id,c.query,c.columnName))
        data_ungrouped = Stemformatics_Dataset.getGeoBiosamplesMetadata_ungrouped(db,dataset_id,c.query,c.columnName)
        temp_data = Stemformatics_Dataset.getGeoBiosamplesMetadata_rowHeaders_for_dropdown(db,dataset_id)
        c.row_headers_for_dropdown = json.dumps(temp_data[0])
        c.chip_id = json.dumps(temp_data[1])

        #get headers for rows for ungrouped data
        headers = []
        for item in data_ungrouped:
            headers.append(item[1])

        #removing duplicate row headers
        row_headers = []
        for header in headers:
            if header not in row_headers:
                row_headers.append(header)

        #get column headers for ungrouped data
        headers = []
        for item in data_ungrouped:
            headers.append(item[0])

        #removing duplicate column headers
        column_headers = []
        column_headers.append("")
        for header in headers:
            if header not in column_headers:
                column_headers.append(header)

        #creating table with md_names as columns
        table = []
        row = []
        count = 0;
        for item in data_ungrouped:
            gsm_id = str(item[0])
            md_name = str(item[1])
            md_value = str(item[2])
            
            if count is len(column_headers)-1:
                table.append(row)
                count = 0
                row = []

            if not row:
                row.append(md_name)

            row.append(md_value)
            count+=1
        table.append(row)

        c.data_ungrouped = json.dumps(table)
        c.ungrouped_column_headers = json.dumps(column_headers)
        c.ungrouped_row_headers = json.dumps(row_headers)
        c.data_to_save = self.request.POST.get('save')
        c.data_to_save_to_annotation_table = self.request.POST.get('save_to_annotation_table')
        c.group_by_check = self.request.POST.get('groupByCheck')
        
        c.result = None

        if c.data_to_save_to_annotation_table:
            # print(c.data_to_save_to_annotation_table)
            data_to_save_to_annotation_table = json.loads(c.data_to_save_to_annotation_table)
            result = Stemformatics_Dataset.save_to_annotation_table(db,data_to_save_to_annotation_table,c.ds_id,c.group_by_check)

        if c.data_to_save:
            data_to_save = json.loads(c.data_to_save)
            result = Stemformatics_Dataset.save_updated_geo_biosamples_metadata(db,data_to_save,c.ds_id,c.group_by_check)

        return self.deprecated_pylons_data_for_view













