<%inherit file="../default.html"/>
<%namespace name="Base" file="../base.mako"/>

<%def name="includes()">
	
	
    <!-- <link rel="stylesheet" type="text/css" href="${h.url('/css/admin/annotate_dataset.css')}" > -->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
   	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
   	
   	<script src="https://cdnjs.cloudflare.com/ajax/libs/handsontable/4.0.0/handsontable.min.js"></script>
   	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/handsontable/4.0.0/handsontable.min.css"/>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="${h.url('/css/jquery.handsontable.css')}" >
    <script type="text/javascript" src="${h.url('/js/jquery.handsontable.js')}"></script> -->
    <!-- <script type="text/javascript" src="${h.url('/js/admin/annotate_dataset.js')}"></script> -->
    
    <script type="text/javascript" src="${h.url('/js/keymaster.js')}"></script>
</%def>


<head>
	<style>
	.handsontable col.rowHeader {
	    width: 300px !important;
	}
	.btn {
		display:inline-block;
	    background-color: rgb(253,127,38);
	    border: none;
	    color: white;
	    padding: 6px 15px;
    	text-align: center;
	    cursor: pointer;
	    font-size: 16px;
	    height:34px;
	    line-height: 5px;
	}
	/* Darker background on mouse-over */
	.btn:hover {
	    background-color: rgb(233,107,38);
	}
	.controls{
		float:left;
		margin-right:10px;
	}
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input {display:none;}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: rgb(253,127,38);
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}

	#groupBy_text{
		display: table;
		color:gray;
		vertical-align: middle;
		line-height: 34px;
		align-content: center;
		margin-right:1px !important;
		font-family: sans-serif;
	}
	/*#save_button{
		float:right !important;
	}
	#saveToAnnotationTable{
		float:right !important;
	}
	#reload_button{
		float:right !important;
	}
	#exportCSV,#exportJSON{
		float:right !important;
	}*/
	#groupBy_checkbox{
		/*margin-left: 15px;
		margin-right:15px;*/
	}
	#control_menu{
		margin-bottom:20px;
		height : 34px;
	}
	#loading_save,#loading_filter,#loading_save_to_annotation_table{
		color:rgb(253,127,38);
		/*float:right; !important;*/
		line-height : 28px;
		padding-top:5px;
	}
	.container{
		/*border : 1px solid red;*/
	}
	.d-flex{
		/*border : 1px solid red;*/
	}
	select.form-control:not([size]):not([multiple]) {
    	height: calc(2rem + 2px) !important;
	}
	#search_bar,#selection{
		padding:0px;
	}
	#filter{
		padding-right:0px;
		padding-left:0px;
	}
	/*#loading_filter,#backToStartSubmitBtn,#readmebtn{
		margin-left:8px;
	}*/
	#instructions{
		margin-bottom:20px;
		border: 1px solid lightgray;
		padding : 10px 20px 0px 30px;
	}
	#readme_list{
		list-style-type: decimal;
	}
	#close_div{
		position: absolute;
    	top: 0px;
    	right: 5px;	
	}
	.instruct_class{
		position: relative;
    	display: inline-block;
	}
	</style>
</head>

<div id="data_grouped" class="hidden">${c.data_grouped}</div>
<div id="data_ungrouped" class="hidden">${c.data_ungrouped}</div>
<div id="data_save" name="data_save" class="hidden">${c.result}</div>
<div id="get_ds_id" class="hidden">${c.ds_id}</div>
<div id="get_ungrouped_column_headers" class="hidden">${c.ungrouped_column_headers}</div>
<div id="get_ungrouped_row_headers" class="hidden">${c.ungrouped_row_headers}</div>
<div id="get_row_headers_for_dropdown" class="hidden">${c.row_headers_for_dropdown}</div>
<div id="get_chip_id" class="hidden">${c.chip_id}</div>
<div id="get_column_name" class="hidden">${c.columnName}</div>
<div id="get_query" class="hidden">${c.query}</div>

<div class="d-flex justify-content-start">
<div id="instructions" class="instruct_class d-none">
	<button type="button" id="close_div" class="close" aria-label="Close">
  		<span aria-hidden="true">&times;</span>
	</button>
	<ol id="readme_list">
		<li>Filter : Enter your query here</li>
		<li>Choose Column : Choose a column in which you would like to search the query to the left</li>
		<li>Search : After filling in the query and choosing a column, this button needs to be clicked to begin search</li>
		<li>Back To Start : takes you back to the page with the table for the mentioned ds_id</li>
		<li>Readme : Shows this readme</li>
		<li>Save : Save data to geo_biosamples_metadata table in the database</li>
		<li>(Save) Annotation Table : Saves data to biosamples_metadata table in the database</li>
		<li>Clear Changes : If changes are made in the table and save hasn't been clicked, this button would refresh and clear the unsaved changes</li>
		<li>CSV/JSON : Downloads data displayed on the screen at the time in the according format</li>
	</ol>
</div>
</div>

<form id="searchByColumn" action="#" method="POST">
	<div class="d-flex justify-content-start">
		
		<input class="hidden" type="text" name="ds_id" value="${c.ds_id}">

		<div id="filter" class="input-group mb-3 col-md-3 mr-auto">
			<div class="input-group-prepend">
				<span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-filter"></i></span>
			</div>
			<input name="query" id="queryForColumnSearch" type="text" class="form-control" placeholder="Filter">
		</div>

		<div id="selection" class="form-group col-md-4 mr-auto">
			<select name="columnName" id="columnSelect" class="form-control">
				<option>Choose Column...</option>
			</select>
		</div>

		<button id="columnSearchSubmit" class="btn mr-auto" type="submit">Search</button>

		<div id="loading_filter" class="controls hidden mr-auto">
			<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
		</div>

		<button id="backToStartSubmitBtn" class="btn mr-auto" type="button">Back To Start</button>

		<button id="readmebtn" class="btn mr-auto" type="button">Readme</button>

	</div>
</form>

<form id="backToStart" action="#" method="POST">
	<input type="submit" id="backToStartInput" class="hidden" type="text" name="ds_id" value="${c.ds_id}">
</form>

	<div class="d-flex justify-content-start">
		<!-- <div id="search_bar" class="controls mr-auto">
			<input id="search" placeholder="Searching" type="text"><br><br>
		</div> -->
		<div id="search_bar" class="input-group mb-3 col-md-3 mr-auto">
			<div class="input-group-prepend">
				<span class="input-group-text" id="inputGroup-sizing-sm"><i class="fa fa-search"></i></span>
			</div>
			<input id="search" type="text" class="form-control" placeholder="Quick Search in Table">
		</div>

		<div id="groupBy_checkbox" class="controls mr-auto">
			<div id="groupBy_text" class="controls">Group : </div>
			<label class="switch">
			  <input type="checkbox" id="groupBy">
			  <span class="slider round"></span>
			</label>
		</div>

		<div id="save_button" class="controls mr-auto">
			<button class="btn" name="save" id="save"><i class="fa fa-save"></i> Save</button>
		</div>

		<div id="loading_save" class="controls hidden mr-auto">
			<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
		</div>
		<div id="saveToAnnotationTable" class="controls mr-auto">
			<button class="btn" name="saveToAnnotationTable" id="saveToAnnotationTable"><i class="fa fa-save"></i> Annotation Table</button>
		</div>

		<div id="loading_save_to_annotation_table" class="controls hidden mr-auto">
			<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>
		</div>

		<div id="reload_button" class="controls mr-auto">
			<button class="btn" name="reload" id="reload">Clear Changes</button>
		</div>

		<div id="exportCSV" class="controls mr-auto">
			<button class="btn" id="export_to_excel"><i class="fa fa-download"></i> CSV</button>
		</div>
		<div id="exportJSON" class="controls mr-auto">
			<button class="btn" id="export_to_excel"><i class="fa fa-download"></i> JSON</button>
		</div>		

	</div>


	<div id="display_data" class="d-flex justify-content-center"></div>

<script>

	function alternateRowColor(instance, td, row, col, prop, value, cellProperties){
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		td.style.backgroundColor = '#fcf1eb';
	}

	var withoutChange_grouped = []
	var withChange_grouped = []
	function getChangesGrouped(instance,original,changed){
		withoutChange_grouped.push(original)
		withChange_grouped.push(changed)
	}

	var withoutChange_ungrouped = []
	var withChange_ungrouped = []
	function getChangesUngrouped(instance,original,changed){
		withoutChange_ungrouped.push(original)
		withChange_ungrouped.push(changed)
		var beforeChange = JSON.stringify(withoutChange_ungrouped);
		var afterChange = JSON.stringify(withChange_ungrouped);
	}

	function showData(geo_biosamples_metadata){
		
		if(geo_biosamples_metadata.length == 0){
			alert("Please Enter Valid ds_id/sample_type");
		}
		else{
			var hot = $("#display_data").handsontable({
				data: geo_biosamples_metadata,
				viewportColumnRenderingOffset : 100,
	            viewportRowRenderingOffset : 100,
	            height:400,
	          	stretchH: 'all',
	            colWidths:250,
	            rowHeights:50,
				sortIndicator:false,
			});

			Handsontable.dom.addEvent(document.getElementById('search'), 'keyup', function() {
	        	filter(('' + this.value).toLowerCase(),hot);
	    	});
		}
	    
	}

	function filter(search,instance) {
        clearTimeout(delayTimer);
        var delayTimer = setTimeout(function() {
           	var row, r_len;
            var data_temp = geo_biosamples_metadata;
            var array = [];
            for (row = 0, r_len = data_temp.length; row < r_len; row++) {
                for(col = 0, c_len = data_temp[row].length; col < c_len; col++) {
                    // console.log(data_temp[row][col] + " " + search);
                    if(('' + data_temp[row][col]).toLowerCase().indexOf(search) > -1) {
                        array.push(data_temp[row]);
                        break;
                    }
                }
            }
            instance.handsontable("loadData",array);
        }, 250); // Will do the ajax stuff after 1000 ms, or 1 s
	}

	var handsontable2csv = {
	    string: function(instance) {
	        var headers = instance.getColHeader();
	        
	        var csv = "sep=;\n"
	        csv += headers.join(";") + "\n";
	        
	        for (var i = 0; i < instance.countRows(); i++) {
	            var row = [];
	            for (var h in headers) {
	                var prop = instance.colToProp(h)
	                var value = instance.getDataAtRowProp(i, prop)
	                row.push(value)
	            }
	            
	            csv += row.join(";")
	            csv += "\n";
	        }
	        
	        return csv;
	    },
	    
	    download: function(instance, filename) {
	        var csv = handsontable2csv.string(instance)

	        var link = document.createElement("a");
	        link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(csv));
	        link.setAttribute("download", filename);

	        document.body.appendChild(link)
	        link.click();
	        document.body.removeChild(link)
	    }
	}

	var handsontable2json = {
		string: function(instance) {
			json = JSON.stringify({data: instance.getData()})
	        return json;
	    },

		download: function(instance, filename) {
	        var json = handsontable2json.string(instance)

	        var link = document.createElement("a");
	        link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(json));
	        link.setAttribute("download", filename);

	        document.body.appendChild(link)
	        link.click();
	        document.body.removeChild(link)
	    }
	}

	function displayUngroupedData(){
		geo_biosamples_metadata = jQuery.parseJSON($("#data_ungrouped").html());
		showData(geo_biosamples_metadata);
		hotInstance = $("#display_data").handsontable('getInstance');
		hotInstance.updateSettings({
			colHeaders:jQuery.parseJSON($(get_ungrouped_column_headers).html()),
			columnSorting: false,
			fixedColumnsLeft:1,
			cells: function (row, col, prop) {
				var cellProperties = {};
				if (col===0) {
					cellProperties.readOnly = true;
				}
				if (row % 2 == 0) {
            		cellProperties.renderer = alternateRowColor;
        		}
				return cellProperties;
			},
			afterChange: function(changes, src) {
			    if (src !== 'loadData' && $("#groupBy").prop('checked')===false) {
			    	var str = this.getDataAtRow(changes[0][0]);
			    	var hotInstance = $("#display_data").handsontable("getInstance");
			    	var colHeaderList = hotInstance.getColHeader();
			    	var gsm_id = colHeaderList[changes[0][1]];
			    	var md_name = this.getDataAtRow(changes[0][0])[0];
			    	var original = [gsm_id,md_name,changes[0][2]];
			    	var changed = [gsm_id,md_name,changes[0][3]];
			    	getChangesUngrouped(this,original,changed)
			    }
			}
		});
		return false;
	}

	function displayGroupData(){
		geo_biosamples_metadata = jQuery.parseJSON($("#data_grouped").html());
		showData(geo_biosamples_metadata);
		hotInstance = $("#display_data").handsontable('getInstance');
		hotInstance.updateSettings({
			colHeaders:['gsm_id','md_name','md_value','count'],
			rowHeaders:false,
			fixedColumnsLeft:false,
			colWidths: [85, 80, 80, 80],
			columnSorting: {
				column: 1,
				sortOrder: 'asc'
			},
			cells: function (row, col, prop) {
			var cellProperties = {};
				if (col===0 || col===1 || col==3) {
					cellProperties.readOnly = true;
				}
				if (row % 2 == 0) {
            		cellProperties.renderer = alternateRowColor;
        		}
				return cellProperties;
			},
			afterChange: function(changes, src) {
			    if (src !== 'loadData' && $("#groupBy").prop('checked')===true) {
			        var str = this.getDataAtRow(changes[0][0]);
			        var md_name = str[1];
			        var count = str[3];
			        var original = [md_name,changes[0][2],count];
			        var changed = [md_name,changes[0][3],count];
			        getChangesGrouped(this,original,changed)
			    }
			}
		});
		return true;
	}

	function addSelectOptionsToFilterDropdown(){
		var rowHeaders = jQuery.parseJSON($(get_row_headers_for_dropdown).html());
		for(var i=0;i<rowHeaders.length;i++){
			var select_option=$('<option>'+[rowHeaders[i]]+'</option>');
			select_option.appendTo('#columnSelect');
		}
		var columnName = $("#get_column_name").html();
		if(columnName==""){
			columnName = "Choose Column...";
		}
		$('select option:contains('+columnName+')').prop('selected',true);
		var query = $("#get_query").html();
		$("#queryForColumnSearch").val(query);
	}

	function checkForChangesInTable(){
		var data_to_save=[];
		if($("#groupBy").prop('checked')===true){
			if(withoutChange_grouped.length!=0 && withChange_grouped.length!=0){
				data_to_save.push(withoutChange_grouped);
				data_to_save.push(withChange_grouped);
			}
			else{
				data_to_save = null;
			}
		}
 		else{
 			if(withoutChange_ungrouped.length!=0 && withChange_ungrouped.length!=0){
 				data_to_save.push(withoutChange_ungrouped);
 				data_to_save.push(withChange_ungrouped);
 			}
 			else{
 				data_to_save = null;
 			}
 		}
 		return data_to_save;
 	}

	function saveToGeoBiosamplesMetadata(groupByCheck){
		var data_to_save = checkForChangesInTable();
		hotInstance = $("#display_data").handsontable('getInstance');

 		if(data_to_save){
 			$('#loading_save').show();
	 		$.ajax({     
			    type: "POST",
			    url: "#",
			    data: {'save':JSON.stringify(data_to_save),'groupByCheck':groupByCheck},
			    cache: false,
			    success: function(){
			    	$('#loading_save').hide();
	                alert("Saved Successfully")
	                location.reload()
	            }
			});
	 	}
	 	else{
	 		alert("No changes made")
	 	}
	}

	function saveToAnnotationTable(){
		var data = jQuery.parseJSON($("#data_ungrouped").html());
		var colHeader = jQuery.parseJSON($('#get_ungrouped_column_headers').html());
		var chip_id = jQuery.parseJSON($(get_chip_id).html());

		var temp1,temp2;
		var data_to_save = [];

		var age="",gender="",organism=""
		for(var i=0;i<data.length;i++){
			temp1 = data[i][0];
			temp2 = temp1.split('.');

			if(temp2[2]=='age'){
				age = data[i];
			}
			else if(temp2[2]=='gender'){
				gender = data[i]; 
			}
			else if(temp2=='organism_ch1'){
				organism = data[i];
			}
		}
		
		for(var i=1;i<colHeader.length;i++){
			if(age!=""){
				data_to_save.push([colHeader[i],"Age",age[i],chip_id[i-1]]);
			}
			if(gender!=""){	
				data_to_save.push([colHeader[i],"Sex",gender[i],chip_id[i-1]]);
			}
			if(organism!=""){
				data_to_save.push([colHeader[i],"Organism",organism[i],chip_id[i-1]]);
			}
		}

		$('#loading_save_to_annotation_table').show();
		$.ajax({     
		    type: "POST",
		    url: "#",
		    data: {'save_to_annotation_table':JSON.stringify(data_to_save)},
		    cache: false,
		    success: function(){
		    	$('#loading_save_to_annotation_table').hide();
                alert("Saved Successfully")
            }
		});
	}

	$(document).ready(function(){
		var groupByCheck = false;
		var hotInstance;

		// initializing
		if(sessionStorage.getItem("groupByCheck") == "true"){
			groupByCheck = displayGroupData();
			$('#groupBy').prop('checked',true)
		}
		else{
			groupByCheck = displayUngroupedData();
			$('#groupBy').prop('checked',false)
		}

    	addSelectOptionsToFilterDropdown();
    	$('#searchByColumn').submit(function(e){
    		var columnName = $('#columnSelect').val();
    		if(columnName == "Choose Column..."){
    			e.preventDefault();
    			alert("Please select column name from dropdown");
    		}
    		else{
    			$('#loading_filter').show();
	    		$.ajax({     	
				    type: "POST",
				    url: $(this).attr('action'),
				    success: function(){
				    	$('#loading_filter').hide();
		            }
				});
			}
    	});

    	$('#backToStartSubmitBtn').click(function(){
    		$('#backToStart').submit();
    	});

    	$('#readmebtn').click(function(){
    		$('#instructions').removeClass("d-none");
    	});

    	$('#close_div').click(function(){
    		$('#instructions').addClass("d-none");
    	});
		
		//slider for groupby or not
		$('#groupBy').change(function () {
      		if (this.checked){	
      			groupByCheck = displayGroupData();
      			sessionStorage.setItem("groupByCheck", groupByCheck);
	        }
	        else {
      			groupByCheck = displayUngroupedData();
      			sessionStorage.setItem("groupByCheck", groupByCheck);
	    	}
    	});

		//save data
		$("#save").click(function(){
			saveToGeoBiosamplesMetadata(groupByCheck);
		});

		//save summary table data to the annotation table
		$("#saveToAnnotationTable").click(function(){
			var check = checkForChangesInTable();
			if(check==null){
				saveToAnnotationTable();
			}
			else{
				alert('Please save to database first');
			}
		});

		//download as csv
    	$("#exportCSV").click(function(){
    		var dataset_id = $("#get_ds_id").html();
    		hotInstance = $("#display_data").handsontable('getInstance');
    		var filename;

    		if (groupByCheck){
        		filename = "ds_id_"+dataset_id+"_grouped.csv";
        	}
        	else{
				filename = "ds_id_"+dataset_id+"_ungrouped.csv";
        	}
        	handsontable2csv.download(hotInstance,filename);
    	});

    	$("#exportJSON").click(function(){
    		var dataset_id = $("#get_ds_id").html();
    		hotInstance = $("#display_data").handsontable('getInstance');
    		var filename;

    		if (groupByCheck){
        		filename = "ds_id_"+dataset_id+"_grouped.json";
        	}
        	else{
				filename = "ds_id_"+dataset_id+"_ungrouped.json";
        	}
        	handsontable2json.download(hotInstance,filename);
    	});

    	$("#reload_button").click(function(){
    		location.reload();
    	});
	});
</script>