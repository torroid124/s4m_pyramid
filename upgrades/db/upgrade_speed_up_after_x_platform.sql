-- removing all the duplicate chip ids for the Matigian dataset
Select * FROM biosamples_metadata 
WHERE ds_id = 2000 and md_name = 'Replicate Group ID' 
AND NOT EXISTS (
SELECT * FROM stemformatics.probe_expressions_avg_replicates as pe_avg 
WHERE chip_type=21 and ds_id = 2000 and probe_id='ILMN_1660306' and biosamples_metadata.md_value = replicate_group_id and biosamples_metadata.chip_id = pe_avg.chip_id
) 
order by md_value;
