
# run this first upgrades/to_v6.5.2/2626_copy_over_assay_platform_fields_to_metastore.sql - this will add mapping_id to datasets table
# psql -U portaladmin portal_prod -f 2626_copy_over_assay_platform_fields_to_metastore.sql

#  delete from dataset_metadata where ds_name in ('probeName','yAxisLabel');


import psycopg2, cPickle
import psycopg2.extras

psycopg2_conn_string="host='localhost' dbname='portal_prod' user='portaladmin'"

def get_all_assay_platforms():
    conn_string = psycopg2_conn_string
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select * from assay_platforms;")
    # retrieve the records from the database
    result_of_all_assay_platforms = cursor.fetchall()

    cursor.close()
    conn.close()
    return result_of_all_assay_platforms


def get_all_dataset_for_a_chip_type(chip_type):
    conn_string = psycopg2_conn_string
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select * from datasets where chip_type = %(chip_type)s;",{"chip_type":chip_type})
    # retrieve the records from the database
    datasets = cursor.fetchall()

    cursor.close()
    conn.close()

    return datasets

def insert_dataset_metadata(ds_id,ds_name,ds_value):

    conn_string = psycopg2_conn_string
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("insert into dataset_metadata (ds_id,ds_name,ds_value) values (%(ds_id)s,%(ds_name)s,%(ds_value)s);",{"ds_id":ds_id,"ds_name":ds_name,"ds_value":ds_value})
    conn.commit()
    cursor.close()
    conn.close()

def insert_data_for_a_dataset(ds_id,ap_row):


    probe_name = ap_row['probe_name']
    y_axis_label = ap_row['y_axis_label']
    mapping_id = ap_row['mapping_id']
    log_2 = ap_row['log_2']


    ds_name = 'probeName'
    ds_value = probe_name
    insert_dataset_metadata(ds_id,ds_name,ds_value)

    ds_name = 'yAxisLabel'
    ds_value = y_axis_label
    insert_dataset_metadata(ds_id,ds_name,ds_value)

    conn_string = psycopg2_conn_string
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("update datasets set log_2 = %(log_2)s, mapping_id = %(mapping_id)s where id = %(ds_id)s;",{"ds_id":ds_id,"log_2":log_2,"mapping_id":mapping_id})
    conn.commit()
    cursor.close()
    conn.close()
 

def check_results():

    conn_string = psycopg2_conn_string
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select count(*),ds_name from dataset_metadata where ds_name in ('probeName','yAxisLabel') group by ds_name;")
    result = cursor.fetchall()
    cursor.close()
    conn.close()

    print result

    conn_string = psycopg2_conn_string
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select count(*),mapping_id from datasets group by mapping_id;")
    result = cursor.fetchall()
    cursor.close()
    conn.close()

    print result






############################## Main code starts here ##################################
result_of_all_assay_platforms = get_all_assay_platforms()

for ap_row in result_of_all_assay_platforms:
    chip_type = ap_row['chip_type']

    
    datasets = get_all_dataset_for_a_chip_type(chip_type)

    for dataset in datasets:
        ds_id =  dataset['id']
        insert_data_for_a_dataset(ds_id,ap_row)
        

check_results()    
