-- add entries in dataset_metadata for button that needs to be shown in dataset summary


INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Glimma Female.L4-L5 vs L6-S1 Volcano","url":"/glimma/Glimma_Female.L4-L5_vs_L6-S1/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Glimma Female.L4-L5 vs L6-S1 MD-Plot","url":"/glimma/Glimma_Female.L4-L5_vs_L6-S1/MD-Plot.html"}') ;

INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Glimma Female L6-S1 vs Male L6-S1 Volcano","url":"/glimma/Glimma_Female_L6-S1_vs_Male_L6-S1/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Glimma Female L6-S1 vs Male L6-S1 MD-Plot","url":"/glimma/Glimma_Female_L6-S1_vs_Male_L6-S1/MD-Plot.html"}') ;


INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"L4-L5 vs L6-S1 Volcano","url":"/glimma/Glimma_L4-L5_vs_L6-S1/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"L4-L5 vs L6-S1 MD-Plot","url":"/glimma/Glimma_L4-L5_vs_L6-S1/MD-Plot.html"}') ;

INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Female L4-L5 vs Male L4-L5 Volcano","url":"/glimma/Glimma_Female_L4-L5_vs_Male_L4-L5/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Female L4-L5 vs Male L4-L5 MD-Plot","url":"/glimma/Glimma_Female_L4-L5_vs_Male_L4-L5/MD-Plot.html"}') ;

INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Male L4-L5 vs L6-S1 Volcano","url":"/glimma/Glimma_Male_L4-L5_vs_L6-S1/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Male L4-L5 vs L6-S1 MD-Plot","url":"/glimma/Glimma_Male_L4-L5_vs_L6-S1/MD-Plot.html"}') ;

INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Female vs Male Volcano","url":"/glimma/Glimma_Female_vs_Male/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7209,'showReportOnDatasetSummaryPage','{"name":"Female vs Male MD-Plot","url":"/glimma/Glimma_Female_vs_Male/MD-Plot.html"}') ;

INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7134,'showReportOnDatasetSummaryPage','{"name":"Volcano Plot","url":"/glimma/volcano.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7134,'showReportOnDatasetSummaryPage','{"name":"MD Plot","url":"/glimma/MD-Plot.html"}') ;

INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7236,'showReportOnDatasetSummaryPage','{"name":"Postnorm PCA Plot","url":"/pca/post_pca/pca.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7236,'showReportOnDatasetSummaryPage','{"name":"Prenorm PCA Plot","url":"/pca/pre_pca/pca.html"}') ;


INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7142,'showReportOnDatasetSummaryPage','{"name":"CMM PCA Plot","url":"/pca/CMM_pca/pca.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7142,'showReportOnDatasetSummaryPage','{"name":"RPKM PCA Plot","url":"/pca/RPKM_pca/pca.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7142,'showReportOnDatasetSummaryPage','{"name":"nonnorm PCA Plot","url":"/pca/non_norm_pca/pca.html"}') ;
INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (7142,'showReportOnDatasetSummaryPage','{"name":"TMM_RPKM PCA Plot","url":"/pca/TMM_RPKM_pca/pca.html"}') ;
