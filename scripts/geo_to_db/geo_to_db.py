import GEOparse
import psycopg2;
import pandas as pd
import sql

gse_identifiers = ['GSE31738', 'GSE22292', 'GSE20402', 'GSE24759', 'GSE25557', 'GSE15283', 'GSE13201', 'GSE21655', 'GSE25970', 'GSE15148', 'GSE20033', 'GSE29397', 'GSE25673', 'GSE20532', 'GSE16590', 'GSE16997'];

ds_ids = ['5019','5001','4000','5003','5016','5008','5005','5023','5002','5013','5018','5022','5012','5015','5006','3000',]
# for identifier in gse_identifiers:
#     data = GEOparse.get_GEO(identifier, destdir="/home/glenn/geo_to_db/")
#     data.phenotype_data.to_csv("/home/glenn/geo_to_db/geo_biosamples_metadata_"+str(identifier)+".tsv", sep="\t")


columns = ["gsm_id","md_name","md_value","ds_id"]
for k in range(0,len(gse_identifiers)):
    with open("geo_biosamples_metadata_"+str(gse_identifiers[k])+".tsv","r") as input1, open("formatted_data.txt","a+") as output:
        list = []
        for line in input1:
            list.append((line.strip('\n')).split('\t'))
        list[0][0] = "gsm_id"
        headers = list[0]
        for i in range(1,len(list)):
            for j in range(1,len(list[i])):
                output.write(str(list[i][0])+"\t"+str(ds_ids[k])+"\t"+str(headers[j])+"\t"+str(list[i][j])+"\n")
    input1.close()
    output.close()

#try:
#    conn = psycopg2.connect("dbname='portal_beta' user='portaladmin'")
#    print("Successfully connected to the database")
#except:
#    print("I am unable to connect to the database")
#
#cur = conn.cursor()
#cur.execute(" " "CREATE TABLE geo_biosamples_metadata(gsm_id TEXT NULL,ds_id TEXT NULL,md_name TEXT NULL,md_value TEXT NULL)" " ")
#conn.commit()
#cur.execute(" " "COPY geo_biosamples_metadata FROM '/home/glenn/geo_to_db/formatted_data.txt'" " ")
#conn.commit()
